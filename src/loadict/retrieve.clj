(ns loadict.retrieve
  (:require [clj-http.client :as client]
            [loadict.api-keys :refer [app-id app-key]]
            [slingshot.slingshot :refer [try+]])
  (:gen-class))

(def base-url "https://od-api.oxforddictionaries.com:443/api/v1/entries/en/")

(defn- make-success [data word] {:success true :data data :word word})

(defn- make-failure [message word] {:success false :message message :word word})

(defn- get-word
  "Try getting given word from the oxford dictionaries api and return a result map with
  success key set to true if the word was succesfully retrieved and false if error has 
  occurred"
  [word]
  (try+
   (make-success
    (:body (client/get (str base-url word)
                       {:headers {:app_id app-id :app_key app-key}}))
    word)
   (catch [:status 404] {:keys [headers]}
     (make-failure (str "Could not retrieve word " word) word))))

(defn get-words [words]
  (doall (map get-word words)))
