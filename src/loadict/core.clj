(ns loadict.core
  (:gen-class)
  (:require [loadict.retrieve :refer [get-words]]
            [loadict.parse :refer [parse]]
            [loadict.render :refer [card->html]]
            [clojure.string :refer [split replace]]
            [clojure.data.csv :as csv]
            [clojure.java.io :as io]))

(def usage "Provide a comma separated list of words as the first argument")

(defn validate-arguments
  [args]
  (and (= (count args) 1)
       (let [arg (first args)]
         (and (string? arg)
              (not (empty? arg))))))

(defn word-data->html-card [word-data]
  (-> word-data
      (parse)
      (card->html)))

(defn response->html-card
  "Construct an Anki card from given successful response"
  [{:keys [word data]}]
  [word (word-data->html-card data)])

(defn report-failures [failures]
  (doseq [failure failures]
    (println (:message failure))))

(defn retrieve [words]
  (group-by :success (get-words words)))

(defn run [words-str]
  (let [words (split words-str #",")
        results (retrieve words)
        cards (map response->html-card (results true))]
    (with-open [writer (io/writer (str "cards.csv"))]
      (csv/write-csv writer cards :separator \, ))
    (report-failures (results false))))


(defn -main
  [& args]
  (if (validate-arguments args)
    (run (first args))
    (println usage)))
