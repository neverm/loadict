(ns loadict.parse
  (:require [clojure.set :refer [rename-keys]]
            [clojure.string :as string]
            [cheshire.core :as json]
            [loadict.retrieve :refer [get-word]])
  (:gen-class))

(defn- parse-sense [sense]
    (-> sense
        (select-keys [:definitions :examples])
        (update :examples #(map :text %))))

(defn- parse-pronunciation [pronunciation]
  (-> pronunciation
      (select-keys [:dialects :phoneticSpelling])
      (update :dialects #(string/join ", " %))))

(defn- transform-grammatical-feature [{:keys [text type]}] (str type ": " text))

(defn- parse-entry [entry]
  (-> entry
      (select-keys [:grammaticalFeatures :senses])
      (update :senses #(map parse-sense %))
      (update :grammaticalFeatures #(map transform-grammatical-feature %))))

(defn- parse-lexical-entry [lex-entry]
  (-> lex-entry
      (rename-keys {:lexicalCategory :grammatical-type})
      (select-keys [:grammatical-type :pronunciations :entries])
      (update :entries #(map parse-entry %))
      (update :pronunciations #(map parse-pronunciation %))))

(defn parse [response-str]
  (-> (:results (json/parse-string response-str true))
      first
      (select-keys [:lexicalEntries :word])
      (update :lexicalEntries #(map parse-lexical-entry %))))

