(ns loadict.render
  (:gen-class)
  (:require [net.cgrand.enlive-html :as html :refer [content first-child defsnippet deftemplate]]))

;; todo: put this somewhere where it belongs
(def template-file
  "loadict/template.html")

(def lex-entry-sel [:#lexical-entries [:.lexical-entry first-child]])
(def pronunciation-sel (into lex-entry-sel [:.pronunciations [:.pronunciation first-child]]))
(def entry-sel (into lex-entry-sel [:.entries [:.entry first-child]]))
(def grammar-feature-sel
  (into entry-sel [:.grammatical-features [:.feature first-child]]))
(def sense-sel (into entry-sel [:.senses [:.sense first-child]]))
(def def-sel (into sense-sel [:.definitions [:.def first-child]]))
(def example-sel (into sense-sel [:.examples [:.example first-child]]))
(def card-sel [:#card-data])

(defsnippet definition-model template-file def-sel
  [definition]
  [html/root] (content definition))

(defsnippet example-model template-file example-sel
  [example]
  [html/root] (content example))

(defsnippet sense-model template-file sense-sel
  [{:keys [definitions examples]}]
  [:.definitions] (content (map definition-model definitions))
  [:.examples] (content (map example-model examples)))

(defsnippet grammatical-feature-model template-file grammar-feature-sel
  [grammatical-feature]
  [html/root] (content grammatical-feature))

(defsnippet entry-model template-file entry-sel
  [{:keys [grammaticalFeatures senses]}]
  [:.grammatical-features]
  (content (map grammatical-feature-model grammaticalFeatures))
  [:.senses] (content (map sense-model senses)))

(defsnippet pronunciation-model template-file pronunciation-sel
  [{:keys [dialects phoneticSpelling]}]
  [:.ipa] (content phoneticSpelling)
  [:.dialects] (content dialects))

(defsnippet lex-entry-model template-file lex-entry-sel
  [{:keys [grammatical-type pronunciations entries]}]
  [:.speech-part] (content grammatical-type)
  [:.pronunciations] (content (map pronunciation-model pronunciations))
  [:.entries] (content (map entry-model entries))
  )

(defsnippet card-model template-file card-sel
  [{:keys [word lexicalEntries]}]
  [:#word] (content word)
  [:#lexical-entries] (content (map lex-entry-model lexicalEntries))
  )

(defn card->html
  [card]
  (apply str (html/emit* (card-model card))))

